<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page isELIgnored="false"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="d"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>


<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css"
	integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS"
	crossorigin="anonymous">
<script
	src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"
	integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k"
	crossorigin="anonymous"></script>





<title>ListSons</title>
</head>

<style>
body {
	background:
		url("https://i.pinimg.com/originals/ba/14/3a/ba143a61e54aba3176a7b9555154ed28.png")
		no-repeat center center fixed;
	-webkit-background-size: cover;
	-moz-background-size: cover;
	-o-background-size: cover;
	background-size: cover;
}
</style>

<body>


	<div class="row">
		<div class="col-md-10">

			<h1 class="text-center text-white">Welcome ${user}</h1>


			<form action="addUser" id="myForm" method="POST">
				<div class="container">
					<div id="login-row"
						class="row justify-content-center align-items-center">
				
				<div class="form-group col-md-4">
					<label for="txt-name">Name:</label> <input id="txt-name"
						type="text" name="name" class="form-control" />
				</div>
				<div class="form-group col-md-4">
					<label for="txt-age">Age:</label> <input id="txt-age" type="number"
						name="age" class="form-control" />
				</div>

				<input type="submit" value="send me" class="btn btn-outline-primary" />
				</div>
				</div>
			</form>
		</div>
	</div>
	</div>
	</div>

	<form align = "center">
		<div  class="container col-md-4">
			<table class="table table-striped" >

				<thead>
					<tr>

						<th scope="col">Name</th>
						<th scope="col">Age</th>
					</tr>
				</thead>
				<d:forEach items="${lista}" var="item">
					<tr>
						<td>${item.name}</td>
						<td>${item.age}</td>
					</tr>
				</d:forEach>
			</table>
		</div>

</form>
	<script type="text/javascript">
		$(document).ready(function() {

			$('#btn-push').click(function() {

			});

		});

		function push() {
			console.log('push')
		}
	</script>
</body>
</html>