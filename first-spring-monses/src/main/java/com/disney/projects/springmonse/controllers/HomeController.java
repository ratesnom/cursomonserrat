package com.disney.projects.springmonse.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.disney.projects.exercise.FirstSon;

@Controller
public class HomeController {

	private List<FirstSon> listSons = new ArrayList<FirstSon>();

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home(Model model) {

		FirstSon firstSon = new FirstSon("monse", 29);

		model.addAttribute("usuario", firstSon);

		return "home";
	}

	@RequestMapping(value = "/addUser", method = RequestMethod.POST)
	public String addUser(@RequestParam("name") String name, @RequestParam("age") int age, Model model) {

		this.listSons.add(new FirstSon(name, age));

		return "home";
	}

	@ModelAttribute("lista")
	public List<FirstSon> retirveListSons() {
		return listSons;
	}

}
