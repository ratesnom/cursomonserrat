<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page isELIgnored="false"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="msj"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>


<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css"
	integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS"
	crossorigin="anonymous">
<script
	src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"
	integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k"
	crossorigin="anonymous"></script>


<script
	src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>


<link rel="stylesheet"
	href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">





<title>Persons Finder</title>
</head>

<body>


 <d:set var="context" value="${pageContext.request.contextPath}" />

	<div class="container">

		<div id="div-error" class="bg-danger">${error}</div>
		<div class="row ">
			<div class="col-md-10">

				<h1>Welcome ${user}</h1>


				<form action="personsFinder" id="myForm" method="POST">


					<div class = "form-group col-md-7">
		<div class="container col-md-6 text-secondary">
		<font color = "maroon" size = 4> 
			<label for="txt-folio"> Folio: </label> <input id="txt-folio"
				type="text" name="folio" class="form-control" placeholder = "Folio" required/>
				</font>
				<font color = "maroon" size = 4> 
			<label for="txt-name"> Name: </label> <input id="txt-name"
				type="text" name="name" class="form-control" placeholder = "Name" required/>
				</font>
		</div>
	</div>
	
	
	<div class = "form-group col-md-7">
	<div class="container col-md-6 text-secondary">
	<input type = "button" id="btn-push" value = "Search"  class="btn btn-outline-danger" >
	
						<div class="spinner-border text-primary" role="status" style="display:none;" id="loading">
						  <span class="sr-only">Loading...</span>
						</div>
	</div>
	</div>
	</div>
	
		<div class = "form-group col-md-12">
	<div class="container col-md-6 text-secondary">
		<div id="div-error" class="bg-danger">${error}</div>
	</div>
	</div>

<div class="row">

			<div class="col-md-6 ">
				
				<div class="card" style="width: 18rem; display:none;" id="card-user">
				  <div class="card-body">
				    <h5 class="card-title" id="card-username"></h5>
				    <p class="card-text" id="card-age">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
				  </div>
				</div>
				
				
				<div class="alert alert-danger" style="display:none;" role="alert" id="error-div">
				</div>
				
			</div>
		</div>
</form>

		
		<script type="text/javascript">
		$(document).ready(function() {

			$('#txt-id-person').focus();
			

			$('#btn-push').click(function() {
				
				$('#loading').show();
				
				$(this).attr("disabled", true);
				
				 $.post("personsFinder",
				    {
				      folio: $('#txt-folio').val(),
				      name: $('#txt-name').val(),
				    },
				    function(data,status){
				      
				      if(data.exist === true){
				    	  $('#card-folio').text(data.user.folio)
				    	  $('#card-name').text(data.user.name)
				    	  $('#card-user').show();
				    	  $('#error-div').hide();
				      }else{
				    	  $('#error-div').html(data.mesage);
				    	  $('#error-div').show();
				    	  $('#card-user').hide();
				      }
				      
				      $('#loading').hide();
				      $(this).attr("disabled", false);
				    });
				 
			
				
				
				
			});

		});

		function push() {
			console.log('push')
		}
	</script>
</body>
</html>