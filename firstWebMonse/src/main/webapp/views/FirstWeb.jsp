<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page isELIgnored="false"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css"
	integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS"
	crossorigin="anonymous">
<script
	src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"
	integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k"
	crossorigin="anonymous"></script>
<title>My First</title>
</head>
<body>



	<h1 align="center">First Servlet</h1>


	<form action="First" method="post">

		<div class="container col-md-4 text-secondary">
			<label for="txt-name"> Name: </label> <input id="txt-name"
				type="text" name="name" class="form-control" />
		</div>

		<div class="container col-md-4 text-secondary">
			<label for="txt-age"> Age: </label> <input id="txt-age" type="text"
				name="age" class="form-control" />
		</div>

		<div class="container col-md-4">
			<input type="submit" value="send" class="btn btn-outline-primary" />
			<input class="btn btn-primary" type="reset" value="Reset">
		</div>
	</form>


	<div class="container alert alert-primary col-md-4 text-info"
		role="alert">${fullData}</div>



</body>
</html>