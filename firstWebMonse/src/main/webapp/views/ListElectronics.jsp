
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page isELIgnored="false"%>

<%@ page isELIgnored="false"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="d"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css"
	integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS"
	crossorigin="anonymous">
<script
	src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"
	integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k"
	crossorigin="anonymous"></script>


<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<title>List Electronics</title>
</head>
<body>
	<hr>
	<div class="col-md-12" align = "center">
		<form action="pageList" id="myForm" method="POST">
			<div class="form-group col-md-4">
				<label for="txt-name">Name Electronic:</label> <input id="txt-name"
					type="text" name="productName" class="form-control" />
			</div>
			<div class="form-group col-md-4">
				<label for="txt-age">Price:</label> <input id="txt-price"
					type="number" name="price" class="form-control" />
			</div>

			<div class="form-group col-md-4">
				<label for="txt-age">Stock:</label> <input id="txt-stock"
					type="number" name="stock" class="form-control" />
			</div>

			<input type="submit" value="send me" class="btn btn-primary" /> 
			<input class="btn btn-primary" type="reset" value="Reset"/>
		

		</form>

	</div>
	
	<form align = "center">
		<div  class="container col-md-4">
			<table class="table" >

				<thead class="thead-dark">
					<tr align="center">

						<th scope="col" >Product</th>
						<th scope="col">Price</th>
						<th scope="col">Stock</th>
					</tr>
				</thead>
				<d:forEach items="${listElectronics}" var="item">
					<tr>
						<td>${item.productName}</td>
						<td>${item.price}</td>
						<td>${item.stock}</td>
					
				</d:forEach>
			</table>
				
		</div>
		</form>
<script type="text/javascript">

$(document).ready(function () {
	
	$('#btn-push').click(function(){
		$('p').fadeIn(50000);
	});
	
	
	
	
});


function push(){
	console.log('push')
}

</body>
</html>