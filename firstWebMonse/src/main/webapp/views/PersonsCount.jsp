<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page isELIgnored="false"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="msj"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>


<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css"
	integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS"
	crossorigin="anonymous">
<script
	src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"
	integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k"
	crossorigin="anonymous"></script>


<script
	src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>


<link rel="stylesheet"
	href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">





<title>Persons</title>
</head>

<body>


 <d:set var="context" value="${pageContext.request.contextPath}" />

	<div class="container">

		<div id="div-error" class="bg-danger">${error}</div>
		<div class="row ">
			<div class="col-md-10">

				<h1>Welcome ${user}</h1>


				<form action="personsCount" id="myForm" method="POST">


					<div class = "form-group col-md-7">
		<div class="container col-md-6 text-secondary">
		<font color = "maroon" size = 4> 
			<label for="txt-folio"> Folio: </label> <input id="txt-folio"
				type="text" name="folio" class="form-control" placeholder = "Folio" required/>
				</font>
		</div>
	</div>
	
	<div class = "form-group col-md-7">
		<div class="container col-md-6 text-secondary">
		<font color = "maroon" size = 4> 
			<label for="txt-name"> Name: </label> <input id="txt-name"
				type="text" name="name" class="form-control" placeholder = "Enter name" required/>
				</font>
		</div>
	</div>
	
	
	<div class = "form-group col-md-7">
	<div class="container col-md-6 text-secondary">
	<input type = "submit" value = "Send"  class="btn btn-outline-danger" >
	<input class="btn btn-outline-danger" type="reset" value="Reset">
	</div>
	</div>
	</div>
	
		<div class = "form-group col-md-12">
	<div class="container col-md-6 text-secondary">
		<div id="div-error" class="bg-danger">${error}</div>
	</div>
	</div>


</form>

				
		<div class = "form-group col-md-12">
			<div class="container col-md-6 text-secondary">
			<table class="table" align="center" id="tableUser">
				<thead>
					<tr>
						<th scope="col" >Folio</th>
						<th scope="col" >Name</th>
					
					</tr>
					<msj:forEach items="${lista}" var="item">
					<tr>
						<td>${item.folio}</td>
						<td>${item.name}</td>
					
					</tr>
					</msj:forEach>					
				</thead>
			</table>
			</div>
		</div>
		
		<script type="text/javascript">

		$(document).ready(function(){
			$("#txt-folio").focus();
			$("#tableUser").DataTable();
			});

		</script>
</body>
</html>