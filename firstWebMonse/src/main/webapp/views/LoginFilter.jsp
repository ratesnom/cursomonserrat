

<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	
	<%@ page isELIgnored="false"%>
	
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="msj"%>

<!DOCTYPE html>
<html>
<head>

<style>
body {
	background:
		url("https://i.pinimg.com/originals/c4/d2/59/c4d259c66607074b92f203d73407c69b.png")
		no-repeat center center fixed;
	-webkit-background-size: cover;
	-moz-background-size: cover;
	-o-background-size: cover;
	background-size: cover;
}
</style>


<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css"
	integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS"
	crossorigin="anonymous">
<script
	src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"
	integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k"
	crossorigin="anonymous"></script>
<script src="http://code.jquery.com/jquery-3.3.1.js"
	integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
	crossorigin="anonymous"></script>
	
	<meta charset="UTF-8">

<title>Login</title>
</head>
<style>
.space {
	padding-top: 20%;
}
</style>
<body>
<msj:set var="context" value="${pageContext.request.contextPath}" />

	<div id="login" class="space">
		<div class="container">
			<div id="login-row"
				class="row justify-content-center align-items-center">
				<div id="login-column" class="col-md-6">
					<div id="login-box" class="col-md-12">
					
						
					
						<form id="myForm" class="form" action="${context}/loginFilter" method="POST">
						
							<div id="div-error" class="bg-danger">${error}</div>

							<h3 class="text-center text-white"><em>Login</em></h3>
							<div class="form-group">

								<label for="username" class="text-white">Username:</label><br>
								<input type="text" name="username" id="username"
									class="form-control" placeholder="username">
							</div>
							<div class="form-group">
								<label for="password" class="text-white">Password:</label><br>
								<input type="password" name="password" id="password"
									class="form-control" placeholder="password">
							</div>
							<div class="form-group">
								<input type="submit" id="btn-psh" class="btn btn-outline-light"
									value="submit">
							
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>

</body>
</html>