

<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	<%@ page isELIgnored="false"%>
	<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="msj"%>
	
<!DOCTYPE html>
<html>
<head>

<style>
body {
	background:
		url("https://i.pinimg.com/originals/5b/8c/85/5b8c853780def283ec9c6f5b62dbe498.png")
		no-repeat center center fixed;
	-webkit-background-size: cover;
	-moz-background-size: cover;
	-o-background-size: cover;
	background-size: cover;
}
</style>

<meta charset="UTF-8">
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css"
	integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS"
	crossorigin="anonymous">
<script
	src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"
	integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k"
	crossorigin="anonymous"></script>
<script src="http://code.jquery.com/jquery-3.3.1.js"
	integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
	crossorigin="anonymous"></script>

<title>Welcome</title>
</head>
<style>
.space {
	padding-top: 20%;
}
</style>
<body>

<div class="row d-block">
    	<div style="text-align: right;width:1650px"> 
    	<msj:set var="context" value="${pageContext.request.contextPath}"/>
        <a href="${context}/logout" class="btn btn-outline-danger">Logout</a>
    	</div>
	</div>
	<div id="login" class="space">
		<h3></h3>
		<div class="container">
			<div id="login-row"
				class="row justify-content-center align-items-center">
				<div id="login-column" class="col-md-6">
					<div id="login-box" class="col-md-12">

						<form id="myForm" class="form" action="${context}/secure/welcome" method="post">

							<h3 class="text-center text-white">Welcome ${user}</h3>
							<div class="form-group">

								
							</div>

						</form>
					</div>


				</div>
			</div>
		</div>
	</div>

</body>
</html>