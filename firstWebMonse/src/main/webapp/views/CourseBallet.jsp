<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	<%@ page isELIgnored="false"%>
<!DOCTYPE html>
<html>
<head>

<style>
body {
	background: url(" https://3.bp.blogspot.com/-rjf86Bq9vE0/T7o_WPmZfRI/AAAAAAAABtQ/1tw5HzOrB_0/s1600/d+1convite31.jpg")
		no-repeat center center fixed;
		-webkit-background-size: cover;
	-moz-background-size: cover;
	-o-background-size: cover;
	background-size: cover;
}
</style>

<meta charset="UTF-8">
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css"
	integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS"
	crossorigin="anonymous">

<script src="http://code.jquery.com/jquery-3.3.1.js"
	integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
	crossorigin="anonymous"></script>
	
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>

<link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css"></link>
	


<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="msj"%>


<title>BalletCourse</title>
</head>
<body>
	<div class="row d-block">
    	<div style="text-align: right;width:1650px"> 
        <a href="LoginCourse" class="btn btn-outline-danger">Logout</a>
    	</div>
	</div>
	
	
	 <div class="form-group" style="text-align: right;width:1650px"> 
	 	<div class="container">
	 	<font color = "fuchsia" size = 6> 
	 	<marquee>Welcome to Baby Ballet</marquee></font>
	 	 <font color = "maroon" size = 4> 
</font>
</div>
</div>
	
	
	<form action="babyBallet" method="post">
	<div id="courseBallet"
				class="row justify-content-center align-items-center">
			

	
	<div class = "form-group col-md-7">
		<div class="container col-md-6 text-secondary">
		<font color = "maroon" size = 4> 
			<label for="txt-nameFather"> Father's name: </label> <input id="txt-nameFather"
				type="text" name="nameFather" class="form-control" placeholder = "Father's name" required/>
				</font>
		</div>
	</div>
	
	<div class = "form-group col-md-7">
		<div class="container col-md-6 text-secondary">
		<font color = "maroon" size = 4> 
			<label for="txt-name"> Name: </label> <input id="txt-name"
				type="text" name="name" class="form-control" placeholder = "Enter baby name" required/>
				</font>
		</div>
	</div>
	
		<div class = "form-group col-md-7">
		<div class="container col-md-6 text-secondary">
		<font color = "maroon" size = 4> 
			<label for="txt-age"> Age: </label> <input id="txt-age"
				type="number" name="age" class="form-control" placeholder = "Enter baby's age" required/>
		</font>
		</div>
	</div>
	
	<div class = "form-group col-md-7">
	<div class="container col-md-6 text-secondary">
	<input type = "submit" value = "Send"  class="btn btn-outline-danger" >
	<input class="btn btn-outline-danger" type="reset" value="Reset">
	</div>
	</div>
	</div>
	
	
		<div class = "form-group col-md-12">
	<div class="container col-md-6 text-secondary">
		<div id="div-error" class="bg-danger">${error}</div>
	</div>
	</div>


</form>

				
		<div class = "form-group col-md-12">
			<div class="container col-md-6 text-secondary">
			<table class="table" align="center" id="tableBallet">
				<thead>
					<tr>
						<th scope="col" >Father's name</th>
						<th scope="col" >Name</th>
						<th scope="col">Age</th>
					</tr>
					<msj:forEach items="${listBallet}" var="item">
					<tr>
						<td>${item.nameFather}</td>
						<td>${item.name}</td>
						<td>${item.age}</td>
					</tr>
					</msj:forEach>					
				</thead>
			</table>
			</div>
		</div>
		
		<script type="text/javascript">

		$(document).ready(function(){
			$("#txt-folio").focus();
			$("#tableBallet").DataTable();
			});

		</script>
		
		
</body>

</html>