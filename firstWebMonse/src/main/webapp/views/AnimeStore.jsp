


<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	<%@ page isELIgnored="false"%>
<!DOCTYPE html>
<html>
<head>

<style>
body {
	background: url("https://images7.alphacoders.com/402/402897.jpg")
		no-repeat center center fixed;
}
</style>

<meta charset="UTF-8">
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css"
	integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS"
	crossorigin="anonymous">
<script
	src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"
	integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k"
	crossorigin="anonymous"></script>

<script src="http://code.jquery.com/jquery-3.3.1.js"
	integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
	crossorigin="anonymous"></script>
	
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<!DOCTYPE html>
<html>
<head>
<style>
.space {
	padding-top: 10%;
}
</style>
<meta charset="UTF-8">
<title>Anime Store</title>
</head>
<body>


	<div class="row d-block">
    <div class="float-md-right">
        <a href="pageLogin" class="btn btn-outline-danger">Logout</a>
    </div>

	</div>
	<div id="login " class="space">
		<h3></h3>
		<div class="container">
			<div id="login-row"
				class="row justify-content-center align-items-center">
				<div id="login-column" class="col-md-6">
					<div id="login-box" class="col-md-12">
						<form id="login-form" class="form" action="animeStore"
							method="post">
							<h3 class="text-center text-dark">Welcome to the anime store</h3>
							<div class="form-group">Choose one of the products</div>
							<div class="form-group">
								<input id="radioOnePiece" type="radio" name="anime" value="opc1"> One Piece
							</div>
							<div class="form-group">
								<input id="radioNanatsu" type="radio" name="anime" value ="opc2"> Nanatsu
							</div>
							<div class="form-group">
								<input id="radioNaruto" type="radio" name="anime" value = "opc3"> Naruto
							</div>

							<div class="form-group">
								<div id=onepiece>One piece has a total of 870 chapters currently</div>
								<div id=onepiece1>The price per chapter is $ 6</div>
								
								<div id=nanatsu>Nanatsu has a total of 30 chapters currently</div>
								<div id=nanatsu1>The price per chapter is $ 12</div>
								
								<div id=naruto>Naruto has total of 720 chapters currently</div>
								<div id=naruto1>The price per chapter is $ 6</div>
							</div>

							<div class="form-group">
								<div id=buy>
									How many chapters do you want to buy? <input type=text
										name="buyStore" size=10 maxlength=10 placeholder="0" >
								</div>
							</div>

							<div class="form-group">
								<input type="submit" id="btn-push"
									class="btn btn-outline-success" value="Buy">
							</div>

							<div>
								The total of your purchase is $ <input type=text name="result"
									size=10 maxlength=10  placeholder="0" value="${result}"  id = "totalBuy"> 
							 </div>
	
						
							<div class="alert alert-light" role="alert"> ${error}  </div>
					
							<script>
								$(document).ready(function() {

									var onepiece = $('#onepiece');
									var onepiece1 = $('#onepiece1');
									var nanatsu = $('#nanatsu');
									var nanatsu1 = $('#nanatsu1');
									var naruto = $('#naruto');
									var naruto1 = $('#naruto1');
									var buy = $('#buy');
								

									onepiece.hide();
									onepiece1.hide();
									buy.hide();
									nanatsu.hide();
									nanatsu1.hide();
									naruto.hide();
									naruto1.hide();
								

									$('#radioOnePiece').click(function() {

										onepiece.show();
										onepiece1.show();

										nanatsu.hide();
										nanatsu1.hide();

										naruto.hide();
										naruto1.hide();
										buy.show();
										totalBuy.show();
									

									});

									$('#radioNanatsu').click(function() {

										onepiece.hide();
										onepiece1.hide();
										nanatsu.show();
										nanatsu1.show();
										naruto.hide();
										naruto1.hide();
										buy.show();

									});

									$('#radioNaruto').click(function() {

										onepiece.hide();
										onepiece1.hide();
										nanatsu.hide();
										nanatsu1.hide();
										naruto.show();
										naruto1.show();
										buy.show();

									});
								});
							</script>

						</form>
						
					
					</div>
				</div>
			</div>
		</div>
	</div>
</body>

</html>