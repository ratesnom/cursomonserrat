package com.disney.web.firstWebMonse.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.disney.projects.exceptions.DuplicateFolioException;
import com.disney.projects.exceptions.NoUserSessionException;
import com.disney.projects.exceptions.TooManyBabyException;
import com.disney.projects.exceptions.ValidateAgeException;
import com.disney.projects.exercise.Ballet;
import com.disney.web.firstWebMonse.utils.ServletUtils;

@WebServlet("/babyBallet")
public class BabyBallet extends HttpServlet {

	private static final int MAX_BABY = 20;
	private static final int MAX_AGE = 10;

	private static final long serialVersionUID = 1L;

	List<Ballet> listBallet = new ArrayList<Ballet>();

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		try {
			ServletUtils.validateSession(req);
			req.getRequestDispatcher("/views/CourseBallet.jsp").forward(req, resp);
		} catch (NoUserSessionException e) {
			e.printStackTrace();
			req.getRequestDispatcher("/views/LoginCourse.jsp");
		}
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String nameFather = (String) req.getParameter("nameFather");
		String name = (String) req.getParameter("name");
		int age = Integer.parseInt(req.getParameter("age"));

		try {
			validateBallet(listBallet);
			validateAge(age);

		
			listBallet.add(new Ballet(nameFather, name, age));
					

		} catch (TooManyBabyException tmbe) {
			tmbe.printStackTrace();
			req.setAttribute("error", tmbe.getMessage());


		} catch (ValidateAgeException vae) {
			vae.printStackTrace();
			req.setAttribute("error", vae.getMessage());
		}

		req.setAttribute("listBallet", listBallet);
		req.getRequestDispatcher("/views/CourseBallet.jsp").forward(req, resp);

	}



	private void validateAge(int age) {
		if (age > MAX_AGE){
			throw new ValidateAgeException("The course is only for children under 10 years old");
		}	
	}



	private void validateBallet(List<Ballet> listBallet) throws TooManyBabyException {
		if (listBallet.size() >= MAX_BABY) {
			throw new TooManyBabyException("The ballet course is full");
		}
	}

}