package com.disney.web.firstWebMonse.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.disney.projects.exercise.Electronics;

@WebServlet("/pageList")
public class ListElectronics extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	List<Electronics> listElectronics = new ArrayList<Electronics>();


	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		request.setAttribute("listElectronics", listElectronics);

		request.getRequestDispatcher("/views/ListElectronics.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String productName = (String)req.getParameter("productName");
		double price = Double.parseDouble(req.getParameter("price"));
		int stock = Integer.parseInt(req.getParameter("stock"));

		listElectronics.add(new Electronics(productName, price, stock ));
		
		req.setAttribute("listElectronics", listElectronics);
		
		req.getRequestDispatcher("/views/ListElectronics.jsp").forward(req, resp);

	}

}
