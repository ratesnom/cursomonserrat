package com.disney.web.firstWebMonse.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet("/animeStore")
public class AnimeStore extends HttpServlet {

	private static final long serialVersionUID = 1L;

	private static final int ONE_PIECE = 870;
	private static final int NANATSU = 30;
	private static final int NARUTO = 720;
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		HttpSession session = req.getSession();
		Object userObject = session.getAttribute("user");
		if (userObject != null) {
			req.setAttribute("user", userObject);
			req.getRequestDispatcher("/views/AnimeStore.jsp").forward(req, resp);

		} else {
			req.getRequestDispatcher("/views/Login.jsp").forward(req, resp);
		}

	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		try {
		
		String anime = req.getParameter("anime"); // "anime" es el name de los radios
		int buyStore = (Integer.parseInt(req.getParameter("buyStore"))); // "buyStore" es el nùmero que ingresan de los																		// capitulos que quieren
		int result = 0; // "result" es el resultado de la multiplicaciòn

		
			if (anime.equals("opc1") && buyStore <= ONE_PIECE ){
				result = 6 * buyStore;
		
			} else if (anime.equals("opc2") && buyStore <= NANATSU) {
				result = 12 * buyStore;
				
			}
			else if (anime.equals("opc3")&& buyStore <= NARUTO) {
				result = 6 * buyStore;
				
			}else {
				req.setAttribute("error", "There is not that number of chapters.");
			}
		
		
		req.setAttribute("result", result);
		req.getRequestDispatcher("/views/AnimeStore.jsp").forward(req, resp);
		
		
		}catch(Exception e){
			
		}
		
		
	}

}
