package com.disney.web.firstWebMonse.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.disney.projects.dto.FindUserDTO;
import com.disney.projects.exercise.User;
import com.google.gson.Gson;

@WebServlet("/personsFinder")
public class PersonsFinder extends HttpServlet {

	private static final long serialVersionUID = 1L;

	private static final int MAX_PEOPLE = 30;

	List<String> lista = new ArrayList<String>();
	private Gson gson = new Gson();

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		req.getRequestDispatcher("/views/PersonsFinder.jsp").forward(req, resp);

	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		Map<Integer, User> listaUser = new HashMap<Integer, User>();
		String output = "";

		try {
			Thread.sleep(20000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		listaUser.put(1, new User("B01", "Sandra"));
		listaUser.put(2, new User("B02", "Patricia"));
		listaUser.put(3, new User("B03", "Mauricio"));
		listaUser.put(4, new User("B04", "Cristian"));
		listaUser.put(5, new User("B05", "Lorena"));
		
		 int idPerson = Integer.valueOf(req.getParameter("idPerson")) ;
		 
		 User user = listaUser.get(idPerson);

		FindUserDTO dto = null;

		if (user != null) {
			dto = new FindUserDTO(user);
		} else {
			dto = new FindUserDTO("User not found");
		}

		output = gson.toJson(dto);

		PrintWriter out = resp.getWriter();

		resp.setContentType("application/json");
		resp.setCharacterEncoding("UTF-8");

		out.print(output);
		out.flush();

	}

}
