package com.disney.web.firstWebMonse.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class First extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public First() {
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		request.getRequestDispatcher("/views/FirstWeb.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		String name=(String)request.getParameter("name");
		String age=(String)request.getParameter("age");
		
		String fullData = "Your name is " + name + " and your age is " + age + " years old";
		
		request.setAttribute("fullData", fullData);
		request.getRequestDispatcher("/views/FirstWeb.jsp").forward(request, response);
	}

}
