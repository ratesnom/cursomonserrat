package com.disney.web.firstWebMonse.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.disney.projects.dto.FindUserDTO;
import com.disney.projects.exceptions.DuplicateFolioException;
import com.disney.projects.exceptions.TooManyBabyException;
import com.disney.projects.exceptions.ValidateAgeException;
import com.disney.projects.exercise.Ballet;
import com.disney.projects.exercise.User;
import com.google.gson.Gson;

@WebServlet("/personsCount")
public class PersonsCount extends HttpServlet {

	private static final long serialVersionUID = 1L;

	private static final int MAX_PEOPLE = 20;
	List<User> lista = new ArrayList<User>();
	

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		req.getRequestDispatcher("/views/PersonsCount.jsp").forward(req, resp);

	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		String folio = (String) req.getParameter("folio");
		String name = (String) req.getParameter("name");
	

		try {
			
			Thread.sleep(20000);

            validateFolio(folio);

            validateTotalPeople(lista);

            lista.add(new User(folio, name));
          
					

		} catch (TooManyBabyException tmbe) {
			tmbe.printStackTrace();
			req.setAttribute("error", tmbe.getMessage());

		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 

		req.setAttribute("lista", lista);
		req.getRequestDispatcher("/views/PersonsCount.jsp").forward(req, resp);

	}

    private void validateFolio(String folio) {

        if (lista.contains(folio)) {
            throw new DuplicateFolioException("Duplicate folio");
        }

    }


	private void validateTotalPeople(List<User> lista) throws TooManyBabyException {
		if (lista.size() >= MAX_PEOPLE) {
			throw new TooManyBabyException("The record is full");
		}
	}

}
