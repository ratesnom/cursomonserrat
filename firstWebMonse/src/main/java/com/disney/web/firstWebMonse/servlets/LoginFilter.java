package com.disney.web.firstWebMonse.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet("/loginFilter")
public class LoginFilter extends HttpServlet{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;


	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		req.getRequestDispatcher("/views/LoginFilter.jsp").forward(req, resp);
	}
	
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		HttpSession session =  req.getSession();
		String username = (String)req.getParameter("username");
		String password = (String)req.getParameter("password");

		if (username.equals("Monse") && password.equals("serrin")) {
			session.setAttribute("user", username);
			req.getRequestDispatcher("/views/PersonsCount.jsp").forward(req, resp);
		} else {
			req.setAttribute("error", "User or pass invalid");
			req.getRequestDispatcher("/views/LoginFilter.jsp").forward(req, resp);
		}
	}
	

}
