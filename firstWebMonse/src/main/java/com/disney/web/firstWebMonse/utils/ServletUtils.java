package com.disney.web.firstWebMonse.utils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.disney.projects.exceptions.NoUserSessionException;

public class ServletUtils {
	
	public static final String USER_KEY = "user";
	
	public static void validateSession(HttpServletRequest request ) throws NoUserSessionException{
	HttpSession session = request.getSession()	;
	Object userObject =  session.getAttribute(USER_KEY);
	
	if (userObject == null ) {
		
			throw new NoUserSessionException("You are not in session");
	}
	
	} 
	

}
