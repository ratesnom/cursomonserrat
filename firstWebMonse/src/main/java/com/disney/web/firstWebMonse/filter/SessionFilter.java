package com.disney.web.firstWebMonse.filter;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.disney.projects.exceptions.NoUserSessionException;
import com.disney.web.firstWebMonse.utils.ServletUtils;;

@WebFilter(filterName = "SessionFilter", urlPatterns = "/secure/*")
public class SessionFilter implements Filter {

	public void init(FilterConfig filterConfig) throws ServletException {

	}
	
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {

		HttpServletRequest httpServletRequest = (HttpServletRequest) request;
		HttpServletResponse httpServletResponse = (HttpServletResponse) response;

		try {
			ServletUtils.validateSession(httpServletRequest);

		} catch (NoUserSessionException e) {
			e.printStackTrace();
			httpServletRequest.getRequestDispatcher("/views/LoginFilter.jsp").forward(httpServletRequest,
					httpServletResponse);
		}

		chain.doFilter(httpServletRequest, httpServletResponse);
	}

	public void destroy() {

	}
}
