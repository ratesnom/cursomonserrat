package com.disney.web.firstWebMonse.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.disney.projects.exceptions.NoUserSessionException;
import com.disney.web.firstWebMonse.utils.ServletUtils;

@WebServlet("/secure/welcome")
public class WelcomeUser extends HttpServlet{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;


	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		try {
			ServletUtils.validateSession(req);
			req.getRequestDispatcher("/views/Welcome.jsp").forward(req, resp);
		} catch (NoUserSessionException e) {
			req.getRequestDispatcher("/views/LoginFilter.jsp").forward(req, resp);
		}
	
	}
	
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		req.getRequestDispatcher("/views/Welcome.jsp").forward(req, resp);
	}

}
