package com.disney.projects.interfaces;

public interface Figurable {
	
	void calculateArea();
	void calculatePerimeter();


}
