package com.disney.projects.inheritage;

import com.disney.projects.interfaces.Figurable;

public class Diamond extends Figure implements Figurable {

	private double side;
	private double diagonalMinor;
	private double diagonalGreater;
	private static final int TOTAL_SIDE = 4;
	private static final int DIAMOND_DIVIDE = 2;

	public Diamond(double side, double diagonalMinor, double diagonalGreater) {
		this.side = side;
		this.diagonalMinor = diagonalMinor;
		this.diagonalGreater = diagonalGreater;
	}

	public double getSide() {
		return side;
	}

	public void setSide(double side) {
		this.side = side;
	}

	public double getDiagonalMinor() {
		return diagonalMinor;
	}

	public void setDiagonalMinor(double diagonalMinor) {
		this.diagonalMinor = diagonalMinor;
	}

	public double getDiagonalGreater() {
		return diagonalGreater;
	}

	public void setDiagonalGreater(double diagonalGreater) {
		this.diagonalGreater = diagonalGreater;
	}

	public void calculateArea() {
		this.setArea((diagonalGreater * diagonalMinor) / DIAMOND_DIVIDE);
	}

	public void calculatePerimeter() {
		this.setPerimeter(TOTAL_SIDE * side);
	}

}
