package com.disney.projects.inheritage;

import java.util.*;

public class AverageList {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		ArrayList<Integer> score = new ArrayList<Integer>();
		
		System.out.println("What is your name? ");
		String name = scanner.next();

		System.out.println("Hi " + name);
		
		
		System.out.println(
				"Enter the numbers with which the average will be obtained and when you have finished enter 0 ");
		int number;
		do {

			System.out.println("Number: ");
			number = scanner.nextInt();

			if (number != 0)
				score.add(number);

		} while (number != 0);

		System.out.println("You entered " + score.size() + " numbers:");
		System.out.println(score);

		double suma = 0;
		for (Integer i : score) {
			suma = suma + i;
		}
		System.out.println("The average is: " + suma / score.size());

		System.out.println("BYE");
		scanner.close();
	}

}
