package com.disney.projects.inheritage;

public class Fishes extends VertebrateAnimals {

	public Fishes(String animal, int timeOfLife) {
		super(animal, timeOfLife);
	}

	@Override
	public void displacement() {
		System.out.println("The displacement of the fish is swimming");
	}

	@Override
	public void body() {
		System.out.println("The body of the fish are flaky");
	}

	@Override
	public String toString() {
		return "Fishes [animal= " + getAnimal() + " timeOfLife = " + getTimeOfLife() + " ]";
	}

}
