package com.disney.projects.inheritage;

public class Mammals extends VertebrateAnimals {

	public Mammals(String animal, int timeOfLife) {
		super(animal, timeOfLife);
	}

	@Override
	public void displacement() {
		System.out.println("The displacement of mammals is walking");
	}

	@Override
	public void body() {
		System.out.println("The body of mammals usually have hair");
	}

	@Override
	public String toString() {
		return "Mammals [animal= " + getAnimal() + " timeOfLife = " + getTimeOfLife() + " ]";
	}

}
