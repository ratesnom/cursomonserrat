package com.disney.projects.inheritage;

import com.disney.projects.interfaces.Animals;

public class VertebrateAnimals implements Animals {

	private String animal;
	private int timeOfLife;

	public VertebrateAnimals() {

	}

	public VertebrateAnimals(String animal, int timeOfLife) {
		this.animal = animal;
		this.timeOfLife = timeOfLife;
	}

	public String getAnimal() {
		return animal;
	}

	public void setAnimal(String animal) {
		this.animal = animal;
	}

	public int getTimeOfLife() {
		return timeOfLife;
	}

	public void setTimeOfLife(int timeOfLife) {
		this.timeOfLife = timeOfLife;
	}

	public void displacement() {

	}

	public void body() {

	}

	@Override
	public String toString() {
		return "VertebrateAnimals [animal=" + animal + ", timeOfLife=" + timeOfLife + "]";
	}

}
