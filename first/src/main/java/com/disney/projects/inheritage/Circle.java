package com.disney.projects.inheritage;

import com.disney.projects.interfaces.Figurable;

public class Circle extends Figure implements Figurable{
	
	public Circle (float radio, float diameter){
		this.radio = radio;
		this.diameter = diameter;
	}

	private double radio;

	public double getRadio() {
		return radio;
	}

	public void setRadio(double radio) {
		this.radio = radio;
	}

	public double getDiameter() {
		return diameter;
	}

	public void setDiameter(double diameter) {
		this.diameter = diameter;
	}

	private double diameter;

	public Circle(double radio, double diameter) {
		this.radio = radio;
		this.diameter = diameter;
	}

	public void calculateArea() {
		this.setArea(Math.PI * Math.pow(radio, 2)); 

	}

	public void calculatePerimeter() {
		this.setPerimeter(Math.PI * diameter);
	}

}
