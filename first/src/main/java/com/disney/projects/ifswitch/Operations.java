package com.disney.projects.ifswitch;

public class Operations {

	public static void triangle(double b, double h, double side) {
		double perimeter, area;

		perimeter = side + b + h;
		area = (b * h) / 2;

		System.out.println("The perimeter of the triangle is: " + perimeter);
		System.out.println("The area of the triangle is: " + area);

	}

	public static void squard(double side) {
		double perimeter, area;

		perimeter = 4 * side;
		area = Math.pow(side, 2);

		System.out.println("The perimeter of the squard is: " + perimeter);
		System.out.println("The area of the squard is: " + area);

	}

	public static void rectangle(double b, double h) {
		double perimeter, area;

		perimeter = (2 * b) + (2 * h);
		area = b * h;

		System.out.println("The perimeter of the rectangle is: " + perimeter);
		System.out.println("The area of the rectangle is: " + area);

	}

}
