package com.disney.projects.ifswitch;

import java.util.Scanner;

public class IfStructure {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		String retry = null;

		do {

			System.out.println("Choose a type of triangle to know its description: ");
			System.out.println("1: Isosceles triangle");
			System.out.println("2: Equilateral triangle");
			System.out.println("3: Scalene triangle	");

			int option = scanner.nextInt();

			if (option == 1) {
				System.out.println("The isosceles triangle has 2 equal sides.");
			} else if (option == 2) {
				System.out.println("The sides of the equilateral triangle are equal.");
			} else if (option == 3) {
				System.out.println("The scalene triangle has all its different sides.");
			} else {
				System.out.println("The option is invalid, enter a correct");
			}

			System.out.println();
			System.out.println("Try again? Y/N :");
			retry = scanner.next();

		} while (retry.equalsIgnoreCase("Y"));

		System.out.println("BYE");

		scanner.close();
	}

}
