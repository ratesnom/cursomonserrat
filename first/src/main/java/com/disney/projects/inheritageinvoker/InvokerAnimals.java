package com.disney.projects.inheritageinvoker;

import java.util.ArrayList;
import java.util.List;

import com.disney.projects.inheritage.Fishes;
import com.disney.projects.inheritage.Mammals;
import com.disney.projects.inheritage.VertebrateAnimals;

public class InvokerAnimals {

	public static void main(String[] args) {
		VertebrateAnimals vertebrateAnimals = new VertebrateAnimals("Animals", 1000);

		System.out.println("********* Mammal information **********");
		Mammals mammals = new Mammals("Dog", 15);
		mammals.displacement();
		mammals.body();

		System.out.println("");
		System.out.println("********* Fishes infomartion **********");
		Fishes fishes = new Fishes("Beta Fish", 3);
		fishes.displacement();
		fishes.body();

		System.out.println("");
		System.out.println("********** Animals Examples ***********");
		System.out.println(vertebrateAnimals);
		System.out.println(mammals);
		System.out.println(fishes);

		List<VertebrateAnimals> vertebrate = new ArrayList<VertebrateAnimals>();

		vertebrate.add(vertebrateAnimals);
		vertebrate.add(mammals);
		vertebrate.add(fishes);

		System.out.println("");
		System.out.println("************ Complete ***********");
		System.out.println(vertebrate);


	}

}
