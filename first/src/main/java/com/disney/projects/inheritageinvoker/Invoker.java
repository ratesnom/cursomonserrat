package com.disney.projects.inheritageinvoker;

import com.disney.projects.inheritage.Circle;
import com.disney.projects.inheritage.Diamond;

public class Invoker {

	public static void main(String[] args) {
		Circle circle = new Circle(3, 6);

		circle.calculateArea();
		circle.calculatePerimeter();

		System.out.println("The results of operation for the Circle: ");
		System.out.println("Area = " + circle.getArea());
		System.out.println("Perimeter = " + circle.getPerimeter());

		Diamond diamond = new Diamond(12, 4, 9);

		diamond.calculateArea();
		diamond.calculatePerimeter();
		System.out.println();
		System.out.println("The results of operation for the Diamond: ");
		System.out.println("Area = " + diamond.getArea());
		System.out.println("Perimeter = " + diamond.getPerimeter());
	}

}
