package com.disney.projects.abstracts;

import java.util.Scanner;

public class Atm {

	public static void main(String[] args) {

		int rode;
		int ticket1000 = 0;
		int ticket500 = 0;
		int ticket200 = 0;
		int ticket100 = 0;
		int ticket50 = 0;
		int ticket20 = 0;
		int currency10 = 0;
		int currency5 = 0;
		int currency2 = 0;
		int currency1 = 0;

		Scanner scanner = new Scanner(System.in);
		String retry = null;

		System.out.println("Hi ");

		do {
			System.out.println("How much money do you want to withdraw? ");
			rode = scanner.nextInt();

			if (rode > 10000) {
				System.out.println("You can not withdraw more than $ 10,000");

			} else {
				ticket1000 = rode / 1000; 
				ticket500 = (rode % 1000) / 500; 
				ticket200 = (rode % 1000) % 500 / 200; 
				ticket100 = (rode % 1000) % 500 % 200 / 100;
				ticket50 = (rode % 1000) % 500 % 200 % 100 / 50;
				ticket20 = (rode % 1000) % 500 % 200 % 100 % 50 / 20;
				currency10 = (rode % 1000) % 500 % 200 % 100 % 50 % 20 / 10;
				currency5 = (rode % 1000) % 500 % 200 % 100 % 50 % 20 % 10 / 5;
				currency2 = (rode % 1000) % 500 % 200 % 100 % 50 % 20 % 10 % 5 / 2;
				currency1 = (rode % 1000) % 500 % 200 % 100 % 50 % 20 % 10 % 5 % 2;

				System.out.println("Your cash is :");
				System.out.println(ticket1000 + " ticket of $1000 ");
				System.out.println(ticket500 + " ticket of $500 ");
				System.out.println(ticket200 + " ticket of $200 ");
				System.out.println(ticket100 + " ticket of $100 ");
				System.out.println(ticket50 + " ticket of $50 ");
				System.out.println(ticket20 + " ticket of $20 ");
				System.out.println(currency10 + " currency of $10 ");
				System.out.println(currency5 + " currency of $5 ");
				System.out.println(currency2 + " currency of $2 ");
				System.out.println(currency1 + " currency of $1 ");
			}
			System.out.println();
			System.out.println("Try again? Y/N :");
			retry = scanner.next();

		} while (retry.equalsIgnoreCase("Y"));

		System.out.println("BYE");

		scanner.close();
	}
}
