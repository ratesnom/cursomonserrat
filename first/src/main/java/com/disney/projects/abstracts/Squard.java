package com.disney.projects.abstracts;

public class Squard extends Figures{
	
	private static final int SIDE = 4;

	public Squard(String figure) {
		super(figure, SIDE);
	}

	@Override
	public void showFormula() {

		System.out.println("The formula for perimeter is: ");
		System.out.println("P = 4s");
		System.out.println("Where:");
		System.out.println(" 4 = the sides of the figure");
		System.out.println(" s = the value on the side of the figure");
		System.out.println();
		System.out.println("The formula for area is: ");
		System.out.println("A = s^2");
		System.out.println("Where:");
		System.out.println(" s = the value on the side of the figure");

	}

}
