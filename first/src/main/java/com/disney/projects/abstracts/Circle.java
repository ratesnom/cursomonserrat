package com.disney.projects.abstracts;

public class Circle extends Figures {
	private static final int SIDE = 0;

	public Circle(String figure) {
		super(figure, SIDE);
	}

	@Override
	public void showFormula() {

		System.out.println("The formula for perimeter is: ");
		System.out.println("P = d*π");
		System.out.println("Where:");
		System.out.println(" d = Diameter");
		System.out.println(" π = 3.1416");
		System.out.println();
		System.out.println("The formula for area is: ");
		System.out.println("A = π*r^2");
		System.out.println("Where:");
		System.out.println(" π = 3.1416");
		System.out.println(" r = radio");
	}

}
