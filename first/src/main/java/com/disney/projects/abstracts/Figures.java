package com.disney.projects.abstracts;

public abstract class Figures {
	
	String figure ;
	int side;
	
	public Figures() {
		
	}
	
	public Figures(String figure, int side) {
		this.figure = figure;
		this.side = side;
	}

	public String getFigure() {
		return figure;
	}

	public void setFigure(String figure) {
		this.figure = figure;
	}

	public int getSide() {
		return side;
	}

	public void setSide(int side) {
		this.side = side;
	}
	
	public void information() {
		System.out.println("The " + getFigure());
		System.out.println("Have " + getSide() + " sides");
		showFormula();
	}

	public abstract void showFormula();
}
