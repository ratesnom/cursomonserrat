package com.disney.projects.abstracts;

import java.util.ArrayList;
import java.util.List;

public class InvokerFigure {

	public static void main(String[] args) {
		Squard squard = new Squard("Squard");
		squard.information();
		System.out.println();

		Diamond diamond = new Diamond("Diamond");
		diamond.information();

		System.out.println();

		Circle circle = new Circle("Circle");
		circle.information();

		System.out.println();
		System.out.println("*********************************");
		System.out.println("*          The figures          *");
		System.out.println("*********************************");

		List<Figures> figures = new ArrayList<Figures>();
		figures.add(squard);
		figures.add(diamond);
		figures.add(circle);

		for (Figures figure : figures) {
			figure.information();
			System.out.println("******************");

		}
	}

}
