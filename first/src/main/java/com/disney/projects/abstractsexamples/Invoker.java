package com.disney.projects.abstractsexamples;

import java.util.ArrayList;
import java.util.List;

public class Invoker {

	public static void main(String[] args) {
		Administrator administrator = new Administrator("Denisse");
		administrator.payment();

		System.out.println();

		Teacher teacher = new Teacher("Hannia");
		teacher.payment();

		System.out.println();

		Lawyer lawyer = new Lawyer("Jair");
		lawyer.payment();

		System.out.println();
		System.out.println("*********************************");
		System.out.println("*Today your payroll is deposited*");
		System.out.println("*********************************");

		List<Employee> employees = new ArrayList<Employee>();
		employees.add(administrator);
		employees.add(teacher);
		employees.add(lawyer);

		for (Employee employee : employees) {
			employee.payment();
			System.out.println("******************");

		}
	}

}
