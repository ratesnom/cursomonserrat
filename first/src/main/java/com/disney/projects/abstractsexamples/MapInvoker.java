package com.disney.projects.abstractsexamples;

import java.util.HashMap;
import java.util.Map;

public class MapInvoker {
	public static void main(String[] args) {
		Map<String, Integer> sideFigure = new HashMap<String, Integer>();
		sideFigure.put("Squard", 4);
		sideFigure.put("Hexagon", 6);
		sideFigure.put("Triangle", 3);
		sideFigure.put("Pentagon", 5);

		System.out.println("The squard have " + sideFigure.get("Squard") + " sides");
		System.out.println(sideFigure.get("Hexagon"));
		System.out.println(sideFigure.keySet());
		
		System.out.println("Complete");
		for (String key : sideFigure.keySet()) {
			System.out.println(sideFigure.get(key));
		}
		
		
		System.out.println("Remove");
		sideFigure.remove("Triangle");
		for (String key : sideFigure.keySet()) {
			System.out.println(sideFigure.get(key));
		}
	}

}
