package com.disney.projects.abstractsexamples;

public class Administrator extends Employee{
	
	private static final String JOBTITLE = "Administrator";
	
	
	public Administrator(String name) {
		super(name, JOBTITLE);
	}

	@Override
	public void calculatePayment() {
		
		System.out.println("My salary is $25,000");
	}

}
