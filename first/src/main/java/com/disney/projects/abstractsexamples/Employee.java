package com.disney.projects.abstractsexamples;

public abstract class Employee {

	public Employee() {

	}

	public Employee(String name, String jobTitle) {
		this.name = name;
		this.jobTitle = jobTitle;
	}

	private String name;
	private String jobTitle;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getJobTitle() {
		return jobTitle;
	}

	public void setJobTitle(String jobTitle) {
		this.jobTitle = jobTitle;
	}

	public void payment() {
		System.out.println("Hi " + getName());
		System.out.println("Your job title is " + getJobTitle());
		System.out.println("How much is your pay? ");
		calculatePayment();
	}

	public abstract void calculatePayment();

}
