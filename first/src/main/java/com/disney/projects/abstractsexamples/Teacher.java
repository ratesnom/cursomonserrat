package com.disney.projects.abstractsexamples;

public class Teacher extends Employee{
	
	private static final String JOBTITLE = "Teacher";

	public Teacher(String name) {
		super(name, JOBTITLE);
	}

	@Override
	public void calculatePayment() {
		System.out.println("My salary is $15,000 for four subjects taught ");

	}
	

}
