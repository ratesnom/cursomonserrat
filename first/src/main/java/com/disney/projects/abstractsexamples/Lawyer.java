package com.disney.projects.abstractsexamples;

public class Lawyer extends Employee {

	private static final String JOBTITLE = "Lawyer";

	public Lawyer(String name) {
		super(name, JOBTITLE);
	}

	@Override
	public void calculatePayment() {
		System.out.println("My salary is $27,000 for each case ");

	}

}
