package com.disney.projects.list;

import java.util.*;
import java.util.Scanner;

public class MenuList {

	public static void main(String[] args) {

		Scanner scanner = new Scanner(System.in);
		List<String> subjects = new ArrayList<String>();
		String retry = null;
		String subject = null;

		System.out.println("What is your name? ");
		String name = scanner.next();

		System.out.println("");
		System.out.println("Hi " + name);

		System.out.println("");
		System.out.println("Your subjects are: ");
		subjects.add("Programming");
		subjects.add("Physical");
		subjects.add("English");

		System.out.println(subjects);

		do {
			System.out.println("");
			System.out.println("Choose a option: ");
			System.out.println("1: Add");
			System.out.println("2: Remove");
			System.out.println("3: View");

			int option = scanner.nextInt();

			switch (option) {
			case 1:
				System.out.println("What subject do you want to add? ");
				subject = scanner.next();
				subjects.add(subject);
				break;

			case 2:
				System.out.println("What subject do you want to eliminate? ");
				subject = scanner.next();
				subjects.remove(subject);
				break;

			case 3:
				System.out.println("Your subjects are: ");
				System.out.println(subjects);
				break;

			default:
				System.out.println("Enter a valid option...");
				break;
			}

			System.out.println();
			System.out.println("Try again? Y/N :");
			retry = scanner.next();

		} while (retry.equalsIgnoreCase("Y"));

		System.out.println("BYE");

		scanner.close();

	}

}
