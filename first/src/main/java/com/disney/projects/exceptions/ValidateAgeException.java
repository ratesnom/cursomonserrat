package com.disney.projects.exceptions;

public class ValidateAgeException extends RuntimeException{

	private static final long serialVersionUID = 1L;

	public ValidateAgeException() {
		super();
	}
	
	public ValidateAgeException(String msg) {
		super(msg);
	}
	
	public ValidateAgeException(Throwable throwable) {
		super(throwable);
	}
	
	public ValidateAgeException(String msg, Throwable throwable) {
		super(msg, throwable);
	}
	

}
