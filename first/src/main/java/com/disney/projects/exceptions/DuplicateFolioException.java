package com.disney.projects.exceptions;

public class DuplicateFolioException extends RuntimeException{

	private static final long serialVersionUID = 1L;

	public DuplicateFolioException() {
		super();
	}
	
	public DuplicateFolioException(String msg) {
		super(msg);
	}
	
	public DuplicateFolioException(Throwable throwable) {
		super(throwable);
	}
	
	public DuplicateFolioException(String msg, Throwable throwable) {
		super(msg, throwable);
	}
	

}
