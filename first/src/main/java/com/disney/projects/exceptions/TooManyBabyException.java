package com.disney.projects.exceptions;

public class TooManyBabyException extends Exception{

	private static final long serialVersionUID = 1L;

	public TooManyBabyException() {
		super();
	}
	
	public TooManyBabyException(String msg) {
		super(msg);
	}
	
	public TooManyBabyException(Throwable throwable) {
		super(throwable);
	}
	
	public TooManyBabyException(String msg, Throwable throwable) {
		super(msg, throwable);
	}
	

}
