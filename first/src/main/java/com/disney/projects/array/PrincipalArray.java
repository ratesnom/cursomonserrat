package com.disney.projects.array;

import java.util.Scanner;

public class PrincipalArray {

	public static void main(String[] args) {

		Scanner scanner = new Scanner(System.in);
		String retry = null;
		ArrayOptions arrayOptions = new ArrayOptions();

		do {

			System.out.println("What is your name? ");
			String name = scanner.next();

			System.out.println("How many subjects do you have?: ");
			int TotalSubjects = scanner.nextInt();

			int[] arraySubject = new int[TotalSubjects];
			int count = 0;

			while (count < TotalSubjects) {
				System.out.println("What is your qualification of the subject?: ");
				int qualification = scanner.nextInt();
				arraySubject[count] = qualification;
				count++;
			}

			System.out.println("Hi, " + name);
			System.out.println("Your notes are: ");

			arrayOptions.showScore(arraySubject);
			arrayOptions.average(arraySubject);

			System.out.println();
			System.out.println("Try again? Y/N :");
			retry = scanner.next();

		} while (retry.equalsIgnoreCase("Y"));

		System.out.println("BYE");

		scanner.close();
	}

}
