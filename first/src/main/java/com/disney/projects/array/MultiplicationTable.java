package com.disney.projects.array;

public class MultiplicationTable {

	private int number;
	private int limit;

	public MultiplicationTable() {

	}

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}

	public int getLimit() {
		return limit;
	}

	public void setLimit(int limit) {
		this.limit = limit;
	}

	public void executeTableArray(int[] arrayMultiplication) {
		for (int index = 1; index <= arrayMultiplication.length; index++) {
			arrayMultiplication[index - 1] = getLimit() * index;
			System.out.println(getNumber() + " X " + index + " = " + getNumber() * index);
		}
	}

}
