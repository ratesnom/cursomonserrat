package com.disney.projects.array;

import java.util.Scanner;

public class PrinciplaTableArray {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		String retry = null;
		MultiplicationTable multiplicationTable = new MultiplicationTable();

		do {

			System.out.println("Enter the value of the table to be made: ");
			int number = scanner.nextInt();
			multiplicationTable.setNumber(number);

			System.out.println("Enter the limit to multiply: ");
			int limit = scanner.nextInt();
			multiplicationTable.setLimit(limit);

			System.out.println("The table is: " + multiplicationTable.getNumber());

			int[] arrayMultiplication = new int[limit];
			multiplicationTable.executeTableArray(arrayMultiplication);

			System.out.println();
			System.out.println("Try again? Y/N :");
			retry = scanner.next();

		} while (retry.equalsIgnoreCase("Y"));

		System.out.println("BYE");

		scanner.close();

	}

}
