package com.disney.projects.array;

public class ArrayOptions {

	public void showScore(int[] array) {
		for (int score : array) {
			System.out.println(score);
		}
	}

	public void average(int[] average) {
		double total = 0;

		for (int i = 0; i < average.length; i++) {
			total = total + average[i];
		}

		total = total / average.length;
		System.out.println("Your average is: " + total);

	}

}
