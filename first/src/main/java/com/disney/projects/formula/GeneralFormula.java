package com.disney.projects.formula;

import java.util.Scanner;

public class GeneralFormula {

	/*
	 * x=(-b+-√b2-4ac)/2a
	 * 
	 * x = unknown 
	 * +- = has to add or subtract, therefore there can be two solutions (x1, x2) 
	 * a,b,c = coefficients 
	 * d = √b2-4ac . When it resolves, the result indicates the type of solution of the equation: 
	 * 		If it is positive there are two solutions 
	 * 		If it is 0 there is a solution 
	 * 		If it is negative it has no solution.
	 */

	public static void generalFormula(double a, double b, double c) {
		double d, x1, x2 = 0;

		d = Math.pow(b, 2) - 4 * a * c;

		if (a != 0) {
			if (d < 0) {
				System.out.println("The solution is not real.");
			} else {
				x1 = (-b + Math.sqrt(d)) / (2 * a);
				x2 = (-b - Math.sqrt(d)) / (2 * a);
				System.out.println("The solution is Result1: " + x1 + " , " + "Result2: " + x2);
			}
		} else {
			System.out.println("The coefficient 'a' must be different from 0");

		}

	}

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		String opc = null;

		do {

			System.out.println("Enter coefficient a");
			double a = scanner.nextDouble();

			System.out.println("Enter coefficient b");
			double b = scanner.nextDouble();

			System.out.println("Enter coefficiente c");
			double c = scanner.nextDouble();

			generalFormula(a, b, c);

			System.out.println();
			System.out.println("Try again? Y/N :");
			opc = scanner.next();

		} while (opc.equalsIgnoreCase("Y"));

		System.out.println("BYE");

		scanner.close();

	}
}
