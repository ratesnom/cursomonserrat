package com.disney.projects.loop;

import java.util.Scanner;

public class PrincipalTable {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		String retry = null;
		MultiplicationTable multiplicationTable = new MultiplicationTable();

		do {

			System.out.println("Enter the value of the table to be made: ");
			multiplicationTable.setNumber(scanner.nextInt());

			System.out.println("Enter the limit to multiply: ");
			multiplicationTable.setLimit(scanner.nextInt());

			System.out.println("The table is: " + multiplicationTable.getNumber());

			multiplicationTable.executeTable();

			System.out.println();
			System.out.println("Try again? Y/N :");
			retry = scanner.next();

		} while (retry.equalsIgnoreCase("Y"));

		System.out.println("BYE");

		scanner.close();

	}

}
