package com.disney.projects.loop;

import com.disney.projects.ifswitch.Operations;
import java.util.Scanner;

public class StructureWhile {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		String retry = "y";

		while (retry.equalsIgnoreCase("Y")) {

			System.out.println("Enter value side: ");
			int side = scanner.nextInt();

			System.out.println("Enter value base: ");
			int b = scanner.nextInt();

			System.out.println("Enter value height: ");
			int h = scanner.nextInt();

			System.out.println("Choose a figure to know its perimeter and area: ");
			System.out.println("1: Triangle");
			System.out.println("2: Square");
			System.out.println("3: Rectangle");

			int option = scanner.nextInt();

			switch (option) {
			case 1:
				Operations.triangle(b, h, side);
				break;

			case 2:
				Operations.squard(side);
				break;

			case 3:
				Operations.rectangle(b, h);
				break;

			default:
				System.out.println("Enter a valid option...");
				break;
			}

			System.out.println();
			System.out.println("Try again? Y/N :");
			retry = scanner.next();

		}
		scanner.close();

	}

}
