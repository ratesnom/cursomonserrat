package com.disney.projects.exercise;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Principal {

	public static void main(String[] args) {
		int units = 0;
		Scanner scanner = new Scanner(System.in);
		String retry = null;
		List<ProductStore> productosEnCarrito = new ArrayList<ProductStore>();
		Sale sale = new Sale();

		System.out.println("What is your name? ");
		String name = scanner.next();

		System.out.println("");
		System.out.println("Hi " + name);

		do {
			System.out.println("");
			System.out.println("Choose a option: ");
			System.out.println("1: Show Products");
			System.out.println("2: Buy");

			int option = scanner.nextInt();

			switch (option) {
			case 1:
				System.out.println("These are the products for sale: ");
				sale.showProducts();
				break;

			case 2:

				do {

					System.out.println("What do you want to buy?");
					String productName = scanner.next();

					ProductStore product = sale.lookForProduct(productName);

					if (product != null) {

						System.out.println("How many products will you buy?");
						units = scanner.nextInt();

						if (units < product.getStock()) {
							productosEnCarrito.add(product);

							sale.calculatesTotal(units, product.getPrice());

							System.out.println("Purchase summary " + productosEnCarrito);
							System.out.println("Total purchase: $" + sale.getTotal());
							

						} else {

							System.out.println("There are not so many products in existence ");
						}
					} else {
						System.out.println("There is no product, enter another");
					}

				} while (false);

				break;

			default:
				System.out.println("Enter a valid option...");
				break;
			}

			System.out.println();
			System.out.println("Try again? Y/N :");
			retry = scanner.next();

		} while (retry.equalsIgnoreCase("Y"));

		System.out.println("Thanks for your visit");

		scanner.close();

	}

}
