package com.disney.projects.exercise;

import java.util.ArrayList;
import java.util.List;

public class Sale {

	double total;
	int units;
	int stockTotal;

	List<ProductStore> products = new ArrayList<ProductStore>();

	public Sale() {
		products.add(new Food("Gansito", 10, 30));
		products.add(new Food("Soda", 12, 40));
		products.add(new Food("Water", 10, 20));
		products.add(new Electronics("iPhone", 25000, 15));
		products.add(new Electronics("TV", 12000, 10));
		products.add(new Electronics("Headphones", 7500, 15));
	}

	public ProductStore lookForProduct(String productName) {

		ProductStore productStore = null;

		for (ProductStore productsStore : products) {
			if (productsStore.getProductName().equalsIgnoreCase(productName)) {
				productStore = productsStore;
			}
		}
		return productStore;
	}

	public void showProducts() {
		for (ProductStore productStore : products) {
			System.out.println(productStore);
		}

	}

	public void calculatesTotal(int units, double price) {
		total += units * price;
		if (total > 1000 && total < 3000) {
			double totalFinal = total * 0.30;
			System.out.println("You receive a discount of 30%");
			System.out.println("Total to pay: " + totalFinal);
		} else if (total > 30000) {
			double totalFinal = total * 0.50;
			System.out.println("You receive a discount of 50%");
			System.out.println("*****************************");
			System.out.println("Total to pay: " + totalFinal);
			System.out.println("*****************************");
		}

	}

	public double getTotal() {
		return total;
	}

}
