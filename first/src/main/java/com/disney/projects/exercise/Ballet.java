package com.disney.projects.exercise;

public class Ballet {
	String nameFather;
	String name;
	int age;

	public Ballet() {

	}

	public Ballet(String nameFather, String name, int age) {
		this.nameFather = nameFather;
		this.name = name;
		this.age = age;

	}

	public String getNameFather() {
		return nameFather;
	}

	public void setNameFather(String nameFather) {
		this.nameFather = nameFather;
	}
	
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	@Override
	public String toString() {
		return "Ballet [nameFather=" + nameFather + ", name=" + name + ",  age=" + age + "]";
	}




}
