package com.disney.projects.exercise;

public class User {

	String folio;
	String name;

	public User() {

	}

	public User(String name, String folio) {
		this.folio = folio;
		this.name = name;

	}

	public String getFolio() {
		return folio;
	}

	public void setFolio(String folio) {
		this.folio = folio;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "User [folio=" + folio + ", name=" + name + "]";
	}

}
