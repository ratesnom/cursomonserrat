package com.disney.projects.dto;

import java.io.Serializable;

import com.disney.projects.exercise.User;

public class FindUserDTO implements Serializable {

	/**
	* 
	*/
	private static final long serialVersionUID = -5118501425623079400L;

	private boolean exist;
	private String mesage;
	private User user;

	public FindUserDTO() {
	}

	public FindUserDTO(User user) {
		this.user = user;
		this.exist = true;
	}

	public FindUserDTO(String mesage) {
		this.mesage = mesage;
		this.exist = false;
	}

	public boolean isExist() {
		return exist;
	}

	public void setExist(boolean exist) {
		this.exist = exist;
	}

	public String getMesage() {
		return mesage;
	}

	public void setMesage(String mesage) {
		this.mesage = mesage;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

}
